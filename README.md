# README #

   • Design a model to simulate the traffic between 10th Street and 14th Street during afternoon in Atlanta with Python based on discrete event simulation, including building concept model, simulator developing, model verification and validation. 
   • Compared the distribution of travel time when synchronized and unsynchronized traffic signal are implemented.    