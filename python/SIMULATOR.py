from auto import auto
from signal2 import signal
from lane import lane
from stop import stop
from intersect import intersect
from event import event
import Queue
import numpy


if __name__ == "__main__":

    totaltime = 200
    Now = 0
    pq = Queue.PriorityQueue()

############### INITIAL SET UP ##############
#STEP1: generate dictionary for queues
    laneDict = {}
    #lane(length,intersect,signal, x,y)
    laneDict["st10nlt"] = lane(1000,"st10",0,-0.5)
    laneDict["st10n"] = lane(1000,"st10",0,-0.5)
    laneDict["st10w"] = lane(1000,"st10",-1,0)
    laneDict["st10wlt"] = lane(1000,"st10",-1,0)
    laneDict["st10e"] = lane(1000,"st10",1,0)
    laneDict["st10elt"] = lane(1000,"st10",1,0)  
    laneDict["st1011s"] = lane(1000,"st10",0,0.5)
    laneDict["st1011slt"] = lane(1000,"st10",0,0.5)
    laneDict["st1011n"] = lane(1000,"st11",0,0.5)
    laneDict["st11e"] = lane(1000,"st11",1,1)
    laneDict["st11w"] = lane(1000,"st11",-1,1)
    laneDict["st1112s"] = lane(1000,"st11",0,1.5)
    laneDict["st1112n"] = lane(1000,"st12",0,1.5)
    laneDict["st12w"] = lane(1000,"st12",-1,2)
    laneDict["st12e"] = lane(1000,"st12",1,2)
    laneDict["st1213s"] = lane(1000,"st12",0,2.5)
    laneDict["st1213n"] = lane(1000,"st13",0,2.5)
    laneDict["st1314s"] = lane(1000,"st13",0,3.5)
    laneDict["st13e"] = lane(1000,"st13",1,3)
    laneDict["st1314n"] = lane(1000,"st14",0,3.5)
    laneDict["st1314nlt"] = lane(1000,"st14",0,3.5)  
    laneDict["st14w"] = lane(1000,"st14",-1,4)
    laneDict["st14e"] = lane(1000,"st14",1,4)
    laneDict["st14s"] = lane(1000,"st14",0,4.5)
    laneDict["st14slt"] = lane(1000,"st14",0,4.5)

#STEP2: intialize intersects
    intersectDict = {}
    # intersect(st,slt,nt,nlt,et,elt,wt,wlt,snext,nnext)
    intersectDict["st10"] = intersect(["st10n"],"st10nlt",["st1011s"],"st1011slt",["st10e"],"st10elt",["st10w"],"st10wlt","","st11")
    intersectDict["st14"] = intersect(["st1314n"],"st1314nlt",["st14s"],"st14slt",["st14e"],"",["st14w"],"","st13","")
    intersectDict["st11"] = intersect(["st1011n"],"",["st1112s"],"",["st11e"],"",["st11w"],"","st10","st12")
    intersectDict["st12"] = intersect(["st1112n"],"",["st1213s"],"",["st12e"],"",["st12w"],"","st11","st13")
    intersectDict["st13"] = intersect(["st1213n"],"",["st1314s"],"",["st13e"],"","","","st12","st14")

    queueDict = {}
    queueDict["st10nlt"] = Queue.Queue()
    queueDict["st10n"] = Queue.Queue()
    queueDict["st10w"] = Queue.Queue()
    queueDict["st10wlt"] = Queue.Queue()
    queueDict["st10e"] = Queue.Queue()
    queueDict["st10elt"] = Queue.Queue()
    queueDict["st1011s"] = Queue.Queue()
    queueDict["st1011slt"] = Queue.Queue()
    queueDict["st1011n"] = Queue.Queue()
    queueDict["st11e"] = Queue.Queue()
    queueDict["st11w"] = Queue.Queue()
    queueDict["st1112s"] = Queue.Queue()
    queueDict["st1112n"] = Queue.Queue()
    queueDict["st12w"] = Queue.Queue()
    queueDict["st12e"] = Queue.Queue()
    queueDict["st1213s"] = Queue.Queue()
    queueDict["st1213n"] = Queue.Queue()
    queueDict["st1314s"] = Queue.Queue()
    queueDict["st13e"] = Queue.Queue()
    queueDict["st1314n"] = Queue.Queue()
    queueDict["st1314nlt"] = Queue.Queue()
    queueDict["st14w"] = Queue.Queue()
    queueDict["st14e"] = Queue.Queue()
    queueDict["st14s"] = Queue.Queue()
    queueDict["st14slt"] = Queue.Queue()

#STEP2: generate signal dictionary
    signalDict = {}
    signalDict["st10"] = signal({"0":2,"1":10,"2":38,"3":10,"4":34},["0","2","0","3","0","4","0"],"1")
    signalDict["st11"] = signal({"0":2,"1":0,"2":55,"3":0,"4":41},["0","4","0"],"2")
    signalDict["st12"] = signal({"0":2,"1":0,"2":67,"3":0,"4":29},["0","4","0"],"2")
    signalDict["st14"] = signal({"0":2,"1":15,"2":39,"3":0,"4":40},["0","2","0","4","0"],"1")

#STEP3: generate signal change event
    for key in signalDict:
        signalcurr = signalDict[key]
        timestamp = Now + signalcurr.signaltimeDict[signalcurr.signalphase]
        eventcode = 4
        signalname = key
        lanename = ""
        vid = ""
        newevent = event(timestamp,eventcode,vid,signalname,lanename)
        pq.put((newevent.timestamp,newevent))

    signalDict["st13"] = signal({"0":0,"1":0,"2":0,"3":0,"4":0},[],"")

#STEP4: initialize vehicle enter system event
    VehicleID = 1000
    #trip generation rate at each intersection
    EntrancesRates = {"st10n":3, "st10w":4, "st10e":4, "st11w":9,"st11e":9,"st12w":9,"st12e":9,"st13e":9,"st14w":4,"st14e":4,"st14s":3}
    #entraces xy coordinates
    EntrancesXY = {
    "st10n":{"x": 0, "y": -0.5}, "st10w":{"x": -1, "y": 0}, "st10e":{"x": 1, "y":0},
    "st11w":{"x": -1, "y": 1},"st11e":{"x": 1, "y": 1},
    "st12w":{"x": -1, "y": 2},"st12e":{"x": 1, "y": 2},
    "st13e":{"x": 1, "y": 3},
    "st14w":{"x": -1, "y": 4},"st14e":{"x": 1, "y": 4},"st14s":{"x": 0, "y": 4.5}
    }
    #OD matrix
    Dchoices = {
    "st10n":{"st10w":0.025, "st10e":0.025, "st11w":0.05,"st11e":0.05,"st12w":0.05,"st12e":0.05,"st13e":0.05,"st14w":0.3,"st14e":0.2,"st14s":0.2}, 
    "st10w":{"st10n":0.025, "st10e":0.025, "st11w":0.05,"st11e":0.05,"st12w":0.05,"st12e":0.05,"st13e":0.05,"st14w":0.3,"st14e":0.2,"st14s":0.2},
    "st10e":{"st10w":0.025, "st10n":0.025, "st11w":0.05,"st11e":0.05,"st12w":0.05,"st12e":0.05,"st13e":0.05,"st14w":0.3,"st14e":0.2,"st14s":0.2},
    "st11w":{"st10w":0.025, "st10e":0.025, "st10n":0.05,"st11e":0.05,"st12w":0.05,"st12e":0.05,"st13e":0.05,"st14w":0.3,"st14e":0.2,"st14s":0.2},
    "st11e":{"st10w":0.025, "st10e":0.025, "st11w":0.05,"st10n":0.05,"st12w":0.05,"st12e":0.05,"st13e":0.05,"st14w":0.3,"st14e":0.2,"st14s":0.2},
    "st12w":{"st10w":0.025, "st10e":0.025, "st11w":0.05,"st11e":0.05,"st10n":0.05,"st12e":0.05,"st13e":0.05,"st14w":0.3,"st14e":0.2,"st14s":0.2},
    "st12e":{"st10w":0.025, "st10e":0.025, "st11w":0.05,"st11e":0.05,"st12w":0.05,"st10n":0.05,"st13e":0.05,"st14w":0.3,"st14e":0.2,"st14s":0.2},
    "st13e":{"st10w":0.025, "st10e":0.025, "st11w":0.05,"st11e":0.05,"st12w":0.05,"st12e":0.05,"st10n":0.05,"st14w":0.3,"st14e":0.2,"st14s":0.2},
    "st14w":{"st10w":0.025, "st10e":0.025, "st11w":0.05,"st11e":0.05,"st12w":0.05,"st12e":0.05,"st13e":0.05,"st10n":0.3,"st14e":0.2,"st14s":0.2},
    "st14e":{"st10w":0.025, "st10e":0.025, "st11w":0.05,"st11e":0.05,"st12w":0.05,"st12e":0.05,"st13e":0.05,"st14w":0.3,"st10n":0.2,"st14s":0.2},
    "st14s":{"st10w":0.025, "st10e":0.025, "st11w":0.05,"st11e":0.05,"st12w":0.05,"st12e":0.05,"st13e":0.05,"st14w":0.3,"st14e":0.2,"st10n":0.2}
    }

    #assumed delay times at intersection
    delayDict = {}
    delayDict["tr"] = 2
    delayDict["lt"] = 3
    delayDict["rt"] = 4
    reacttime = 0.5
    headway =1

    #generate vehicles and entersystem event
    autoDict = {}
    for key,value in EntrancesRates.iteritems():
        #generate a new vehicle at intersection %key
        choices = Dchoices[key]
        timestamp = Now + numpy.random.exponential(value)
        #auto(VehicleID, origin, EntrancesXY,choices (destination probability),timestamp)
        newauto = auto(VehicleID, key, EntrancesXY,choices,timestamp)
        vid = str(VehicleID)
        autoDict[vid] = newauto
        
        #generate event
        eventcode = 1
        lanename = newauto.lane
        signalname = newauto.signalname
        newevent = event(timestamp,eventcode,vid,signalname,lanename)
        pq.put((newevent.timestamp,newevent))

        #increase vehicleid
        VehicleID = VehicleID+1

    while Now < totaltime:

        if not pq.empty():

            eventcurr = pq.get()
            Now = eventcurr[0]
            print "\n%s"%Now
            eventcurr = eventcurr[1]

            if eventcurr.no == 1: # Enter system event
                print "event1"
                
                #get current car infromation
                IDcurr = eventcurr.vehID
                autocurr = autoDict[IDcurr]
                s = "vehicle " + "%s" %IDcurr + " enters system from " + "%s"%autocurr.origin
                print s
                
                #schedule next enter system event @ the same entrance and insert it into priority queue
                choices = Dchoices[autocurr.origin]
                timestamp = Now + numpy.random.exponential(EntrancesRates[autocurr.origin])
                newauto = auto(VehicleID, autocurr.origin, EntrancesXY,choices,timestamp)
                vid = str(VehicleID)
                autoDict[vid] = newauto                   
                newevent = event(timestamp,1,vid,newauto.signalname,newauto.lane)
                pq.put((newevent.timestamp,newevent))
                VehicleID = VehicleID+1
               
                #decide wether the vehicle can pass the intersection
                lanecurr = laneDict[autocurr.lane]
                signalcurr = signalDict[lanecurr.intersect]
                intersectcurr = intersectDict[lanecurr.intersect]
                stopcurr = stop(lanecurr,signalcurr,eventcurr.signalname)

                if stopcurr == True:

                    s = "vehicle " + "%s" %IDcurr + " stoped at the intersection" 
                    print s
                    queuecurr = queueDict[autocurr.lane]
                    queuecurr.put(autocurr)
                    queueDict[autocurr.lane] = queuecurr
                

                else:
            
                    deltay = autocurr.dy - autocurr.cy

                    if abs(deltay) <= 0.5: #the vehicle will leave the system after pass this intersection

                        turn = autocurr.turn()
                        autocurr.dtime = Now + delayDict[turn]
                        autoDict[IDcurr] = autocurr
                        s = "vehicle" + "%s" %IDcurr + " left system at time " + "%s" %autocurr.dtime  

                    else: #schedule next arrive event
                        s = "vehicle " + "%s"%IDcurr + " passed intersection " + "%s"%lanecurr.intersect
                        print s

                        if deltay > 0: #going north
                            # print vars(autocurr)                        
                            #update auto information                           
                            turn = autocurr.turn()
                            if turn != "tr":# the vehicle will turn left or right at this intersection
                                autocurr.cy = autocurr.cy + 0.5
                            else:
                                autocurr.cy = autocurr.cy + 1.0
                            autocurr.cx = 0
                            # print vars(autocurr)
                            # print [intersectDict[intersectcurr.nnext].slt, intersectDict[intersectcurr.nnext].st]

                            #determine the lane that the vehicle is going to switch to & update signalname
                            autocurr.setsignalname(intersectDict[intersectcurr.nnext].slt, intersectDict[intersectcurr.nnext].st)
                            # print vars(autocurr)
                            nextlane = autocurr.lane
                            # print nextlane
                            length = laneDict[nextlane].length
                            turn = autocurr.turn()
                            # print vars(autocurr)
                            timestamp = Now + length/autocurr.speed + delayDict[turn]

                            autoDict[IDcurr] = autocurr
                            newevent = event(timestamp,3,IDcurr,autocurr.signalname,autocurr.lane)
                            pq.put((newevent.timestamp,newevent))

                        else: #going south
                             #update auto information
                            
                            turn = autocurr.turn()
                            if turn != "tr":
                                autocurr.cy = autocurr.cy - 0.5
                            else:
                                autocurr.cy = autocurr.cy - 1.0
                            autocurr.cx = 0
                            # print vars(autocurr)
                            # print [intersectDict[intersectcurr.snext].nlt, intersectDict[intersectcurr.snext].nt]
                            autocurr.setsignalname(intersectDict[intersectcurr.snext].nlt, intersectDict[intersectcurr.snext].nt)
                            nextlane = autocurr.lane
                            length = laneDict[nextlane].length
                            turn = autocurr.turn()
                            timestamp = Now + length/autocurr.speed + delayDict[turn]
                           
                            autoDict[IDcurr] = autocurr
                            newevent = event(timestamp,3,IDcurr,autocurr.signalname,autocurr.lane)
                            pq.put((newevent.timestamp,newevent))


            elif eventcurr.no == 2: # Departure event
                
                print "event2"

                lanecurr = laneDict[eventcurr.lanename]
                queuecurr = queueDict[eventcurr.lanename]
                signalcurr = signalDict[lanecurr.intersect]
                intersectcurr = intersectDict[lanecurr.intersect]
                print eventcurr.lanename
                exec( "light = signalcurr." + eventcurr.signalname)

                if queuecurr.empty() or light == "red":

                    continue #do not schedule next departure event

                else:

                    lanecurr = laneDict[eventcurr.lanename]
                    autocurr = queuecurr.get() 
                    deltay = autocurr.dy - autocurr.cy
                    # a

                    if abs(deltay) <= 0.5: 
                        #the vehicle will leave the system after departure this intersection

                        # print "1"
                        turn = autocurr.turn()
                        IDcurr = autocurr.VehicleID
                        autocurr.setdtime(Now + delayDict[turn])
                        autoDict[IDcurr] = autocurr
                        # print vars(autoDict[IDcurr])
                        s = "vehicle" + "%s" %IDcurr + "left system at time " + "%s" %autocurr.dtime  
                        print s
                       
                    elif abs(deltay) == 1 and autocurr.turn() == "tr":

                        # print "2"
                        #the vehicle will leave the system after departure this intersection
                        turn = autocurr.turn()
                        IDcurr = autocurr.VehicleID
                        autocurr.setdtime(Now + delayDict[turn])
                        autoDict[IDcurr] = autocurr
                        s = "vehicle" + "%s" %IDcurr + "left system at time " + "%s" %autocurr.dtime  
                        print s



                    elif deltay >= 1: #going north
                        # print "3"

                        #schedule the next arrival event
                        #update the vehicle information
                        turn = autocurr.turn()
                        if turn == "tr":
                            autocurr.cy = autocurr.cy + 1.0
                        else:
                            autocurr.cy = autocurr.cy + 0.5
                        autocurr.cx = 0

                        # print vars(autocurr)
                        # print [intersectDict[intersectcurr.nnext].slt, intersectDict[intersectcurr.nnext].st]

                        autocurr.setsignalname(intersectDict[intersectcurr.nnext].slt, intersectDict[intersectcurr.nnext].st)
                        # print vars(autocurr)

                        nextlane = autocurr.lane

                        length = laneDict[nextlane].length
                        turn = autocurr.turn()
                        timestamp = Now + length/autocurr.speed + delayDict[turn]
                        autoDict[IDcurr] = autocurr
                        newevent = event(timestamp,3,IDcurr,autocurr.signalname,autocurr.lane)
                        pq.put((newevent.timestamp,newevent))
                        
                        s = "vehicle" + "%s" %IDcurr + "will arrive at next intersect at time " + "%s" %timestamp
                        print s



                    else: #going south
                        #schedule the next arrival event
                        # print "4"
                        turn = autocurr.turn()  
                        if turn == "tr":         
                            autocurr.cy = autocurr.cy - 1.0
                        else:
                            autocurr.cy = autocurr.cy - 0.5
                        autocurr.cx = 0
                        autocurr.setsignalname(intersectDict[intersectcurr.snext].slt, intersectDict[intersectcurr.snext].st)
                        nextlane = autocurr.lane
                        length = laneDict[nextlane].length
                        turn = autocurr.turn()
                        timestamp = Now + length/autocurr.speed + delayDict[turn]
                        autoDict[IDcurr] = autocurr
                        newevent = event(timestamp,3,IDcurr,autocurr.signalname,autocurr.lane)
                        pq.put((newevent.timestamp,newevent))
                        s = "vehicle" + "%s" %IDcurr + " will arrive at next intersect at time " + "%s" %timestamp
                        print s

                    #schedule next departure event for this lane
                    lanename = eventcurr.lanename
                    signalname = eventcurr.signalname
                    vid = ""
                    timestamp = Now + headway#assumed headway is the same for every driver
                    eventcode = 2
                    newevent = event(timestamp,eventcode,vid,signalname,lanename)
                    pq.put((newevent.timestamp,newevent))

            elif eventcurr.no == 3: # Arrival event

                print "event3"

                #get current car infromation
                IDcurr = eventcurr.vehID
                autocurr = autoDict[IDcurr] 
                s = "vehicle " + "%s"%IDcurr + " arrived at intersection " + "%s"%lanecurr.intersect

                #decide wether the vehicle can pass the intersection
                lanecurr = laneDict[autocurr.lane]
                signalcurr = signalDict[lanecurr.intersect]
                intersectcurr = intersectDict[lanecurr.intersect]
                stopcurr = stop(lanecurr,signalcurr,eventcurr.signalname)

                s = "vehicle " + "%s"%IDcurr + " arrived at intersection " + "%s"%lanecurr.intersect
                print s

                if stopcurr:
                    queueDict[autocurr.lane].put(autocurr)
                    s = "vehicle " + "%s"%IDcurr + " stopped at intersection " + "%s"%lanecurr.intersect
                    print s


                else:
                    s = "vehicle " + "%s"%IDcurr + " passed intersection " + "%s"%lanecurr.intersect
                    print s

                    deltay = autocurr.dy - autocurr.cy
                    turn = autocurr.turn()

                    if abs(deltay) <= 0.5: #the vehicle will leave the system after pass this intersection

                        turn = autocurr.turn()
                        autocurr.dtime = Now + delayDict[turn]
                        autoDict[autocurr] = autocurr
                        s = "vehicle " + "%s"%IDcurr + " left system from intersection " + "%s"%lanecurr.intersect + " at time %s"% autocurr.dtime
                        print s


                    elif deltay == 1.0 and autocurr.turn() =="tr" and autocurr.cy == 3.5:
                        turn = autocurr.turn()
                        autocurr.dtime = Now + delayDict[turn]
                        autoDict[autocurr] = autocurr
                        s = "vehicle " + "%s"%IDcurr + " left system from intersection " + "%s"%lanecurr.intersect + " at time %s"% autocurr.dtime
                        print s

                    elif deltay == -1.0 and autocurr.turn() == "tr" and autocurr.cy == 0.5:
                        turn = autocurr.turn()
                        autocurr.dtime = Now + delayDict[turn]
                        autoDict[autocurr] = autocurr
                        s = "vehicle " + "%s"%IDcurr + " left system from intersection " + "%s"%lanecurr.intersect + " at time %s"% autocurr.dtime
                        print s

                    else: #schedule next arrive event
                        if deltay > 0: #going north
                            #update auto information

                            turn = autocurr.turn()
                            if turn != "tr":
                                autocurr.cy = autocurr.cy + 0.5
                            else:
                                autocurr.cy = autocurr.cy + 1.0
                            autocurr.cx = 0

                            autocurr.setsignalname(intersectDict[intersectcurr.nnext].slt, intersectDict[intersectcurr.nnext].st)
                            nextlane = autocurr.lane
                            length = laneDict[nextlane].length
                            timestamp = Now + length/autocurr.speed                            
                            
                            autoDict[IDcurr] = autocurr
                            newevent = event(timestamp,3,IDcurr,autocurr.signalname,autocurr.lane)
                            pq.put((newevent.timestamp,newevent))

                        else: #going south
                            #update auto information
        
                            turn = autocurr.turn()
                            if turn != "tr":
                                autocurr.cy = autocurr.cy - 0.5
                            else:
                                autocurr.cy = autocurr.cy - 1.0

                            autocurr.cx = 0
                           

                            autocurr.setsignalname(intersectDict[intersectcurr.snext].nlt, intersectDict[intersectcurr.snext].nt)
                            nextlane = autocurr.lane
                            length = laneDict[nextlane].length
                            timestamp = Now + length/autocurr.speed
                                                         
                            autoDict[IDcurr] = autocurr
                            newevent = event(timestamp,3,IDcurr,autocurr.signalname,autocurr.lane)
                            pq.put((newevent.timestamp,newevent))   

            else: # Signal Change event

                print "event4"
                #change signal lights and phase code
                signalcurr = signalDict[eventcurr.signalname]
                itersectcurr = intersectDict[eventcurr.signalname]

                signalcurr.setphase()
                signalcurr.setsignal()
                signalDict[eventcurr.signalname] = signalcurr

                #schedule next signal change event
                timestamp = Now + signalcurr.signaltimeDict[signalcurr.signalphase]
                eventcode = 4
                signalname = eventcurr.signalname
                lanename = ""
                vid = ""
                newevent = event(timestamp,eventcode,vid,signalname,lanename)
                pq.put((newevent.timestamp,newevent))

                #let vehicles to move based on the current signal:
                phasecurr = signalcurr.signalphase
                s = "Signal ligth at " +  "%s"%eventcurr.signalname + " changed to phase " + "%s"%phasecurr
                print s
                
                #schedule departure event based on current phase
                if phasecurr == "0": 
                    continue


                if phasecurr == "1":
                    #only ns left turn vehicles may departure (there are vehicles in the queue)
                    intersectcurr = intersectDict[eventcurr.signalname]
                    if not queueDict[intersectcurr.slt].empty():                     
                        #schedule departure event for this lane
                        lanename = intersectcurr.slt
                        signalname = "ltns"
                        vid = ""
                        timestamp = Now + reacttime
                        eventcode = 2
                        newevent = event(timestamp,eventcode,vid,signalname,lanename)
                        pq.put((newevent.timestamp,newevent))

                    if not queueDict[intersectcurr.nlt].empty():  
                        #schedule departure event for this lane
                        lanename = intersectcurr.nlt
                        signalname = "ltns"
                        vid = ""
                        timestamp = Now + reacttime
                        eventcode = 2
                        newevent = event(timestamp,eventcode,vid,signalname,lanename) 

                if phasecurr == "3":
                    #only ew left turn vehicles may departure (there are vehicles in the queue)
                    intersectcurr = intersectDict[eventcurr.signalname]
                    if not queueDict[intersectcurr.elt].empty():                     
                        #schedule departure event for this lane
                        lanename = intersectcurr.elt
                        signalname = "ltew"
                        vid = ""
                        timestamp = Now + reacttime
                        eventcode = 2
                        newevent = event(timestamp,eventcode,vid,signalname,lanename)
                        pq.put((newevent.timestamp,newevent))

                    if not queueDict[intersectcurr.wlt].empty():  

                        #schedule departure event for this lane
                        lanename = intersectcurr.wlt
                        signalname = "ltew"
                        vid = ""
                        timestamp = Now + reacttime
                        eventcode = 2
                        newevent = event(timestamp,eventcode,vid,signalname,lanename) 

                if phasecurr =="2":
                    #only all northbound and south bound vehicles may departure left lane is not considered
                    #assumed no vehicles in the left only lane may departure at this point of time
                    intersectcurr = intersectDict[eventcurr.signalname]
                    stlist = intersectcurr.st
                    ntlist = intersectcurr.nt
                    for st in stlist:
                        if not queueDict[st].empty():                     
                            #schedule departure event for this lane
                            lanename = st
                            signalname = "trns"
                            vid = ""
                            timestamp = Now + reacttime #assumed reaction time is the same for everyone
                            eventcode = 2
                            newevent = event(timestamp,eventcode,vid,signalname,lanename)
                            pq.put((newevent.timestamp,newevent))

                    for nt in ntlist:
                        if not queueDict[nt].empty():  
                            #schedule departure event for this lane
                            lanename = nt
                            signalname = "trns"
                            vid = ""
                            timestamp = Now + reacttime
                            eventcode = 2
                            newevent = event(timestamp,eventcode,vid,signalname,lanename) 

                if phasecurr =="4":
                    #only all east and west bound vehicles may departure left lane is not considered
                    #assumed no vehicles in the left only lane may departure at this point of time
                    intersectcurr = intersectDict[eventcurr.signalname]
                    etlist = intersectcurr.et
                    wtlist = intersectcurr.wt
                    for et in etlist:
                        if not queueDict[et].empty():                     
                            #schedule departure event for this lane
                            lanename = et
                            signalname = "trew"
                            vid = ""
                            timestamp = Now + reacttime #assumed reaction time is the same for everyone
                            eventcode = 2
                            newevent = event(timestamp,eventcode,vid,signalname,lanename)
                            pq.put((newevent.timestamp,newevent))

                    for wt in wtlist:
                        if not queueDict[wt].empty():  
                            #schedule departure event for this lane
                            lanename = wt
                            signalname = "trew"
                            vid = ""
                            timestamp = Now + reacttime
                            eventcode = 2
                            newevent = event(timestamp,eventcode,vid,signalname,lanename) 
        else:
            break

    print"\n -----END OF SIMULATION--------"







                                

