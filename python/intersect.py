class intersect():
    
    """docstring for intersact"""
    #lanename
    st = []
    slt = ""

    nt = []
    nlt = ""

    et = []
    elt = ""

    wt = []
    wlt = ""
    #next intersect at north and south
    nnext=""
    snext=""

    def __init__(self,st,slt,nt,nlt,et,elt,wt,wlt,snext,nnext):
        self.st = st
        self.slt = slt
        self.nt = nt
        self.nlt = nlt
        self.et = et
        self.elt = elt
        self.wt = wt
        self.wlt = wlt
        self.snext = snext
        self.nnext = nnext
        
    def getoppositetr(self,lane):
        #only left turn vheicle has to check opposite through vehicles 
        if lane == self.slt:
            return self.nt
        if lane == self.nlt:
            return self.st
        if lane == self.elt:
            return self.wt
        if lane == self.wlt:
            return self.et

    def getlefttr(self,lane):
        if lane in self.st:
            return self.wt
        if lane in self.nt:
            return self.et
        if lane in self.et:
            return self.st
        if lane in self.wt:
            return self.nt

    def getnextlane(self,lane,turn,intersectDict):
        if turn == "tr":
            if lane in self.st:
                ind = self.st.index(lane)
                if self.nnext != "":
                    return intersectDict[self.nnext].st[ind]
                else:
                    return ""

            if lane in self.nt:
                ind = self.nt.index(lane)
                if self.snext != "":
                    return intersectDict[self.snext].st[ind]
                else:
                    return ""

            if lane in self.et:
                return ""
            if lane in  self.wt:
                return ""

        if turn == "rt":
            if lane in self.st:
                return ""
            if lane in self.nt:
                return ""
            if lane in self.et:
                if self.nnext != "":
                    return intersectDict[self.nnext].st[-1]
                else:
                    return ""
            if lane in self.wt:
                if self.snext != "":
                    return intersectDict[self.snext].nt[-1]
                else:
                    return ""

        if turn == "lt":
            if lane == self.s:
                return self.wnext
            if lane == self.n:
                return self.enext
            if lane == self.e:
                return self.snext
            if lane == self.w:
                return self.nnext