READ ME
Simulator Description for Group 10
1. The simulator is developed using Python 2.7 and is tested on a Mac Computer.
2. To run the simulator, python libaray Numpy is required.
3. To run the simulator, open the python script "SIMULATOR.py" and then run the code. The script will print the time stamp the event that is handled. An example is as follow:
-----------print out examples -------------------------
199.833140011 --> time stamp
event1 --> [event type: 1 is enter system event, 2 is departure event, 3 is arrival at intersection event, and 4 is signal change event]
vehicle 1433 enters system from st10w --> descriptions of the event for debugging.
vehicle 1433 stoped at the intersection --> descriptions of the event for debugging.

198 --> time stamp
event4 --> [event type: 1 is enter system event, 2 is departure event, 3 is arrival at intersection event, and 4 is signal change event]
Signal ligth at st10 changed to phase 0 --> descriptions of the event for debugging.