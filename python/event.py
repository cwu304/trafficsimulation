#event
class event():
    
    timestamp = 0;
    no = 0; #1 enter system, 2 departure, 3 arrival, 4 signal
    vehID = ""
    signalname = ""
    lanename = ""

    def __init__(self,timestamp,no,vehID,signalname,lanename):
        self.timestamp = timestamp
        self.no = no;
        self.vehID = vehID
        self.signalname = signalname;
        self.lanename = lanename;

    def settimestamp(self,timestamp):
        self.timestamp = timestamp;

    def setno(self,no):
        self.no = no;

    def setvehID(self,vehID):
        self.vehID = vehID;

    def setsignalname(self,signalname):
        self.signalname = signalname;

    def setlanename(self,lanename):
        self.lanename = lanename;

