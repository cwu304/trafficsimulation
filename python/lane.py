#Lane
import Queue


class lane():
	
	queue = Queue.Queue()
	# lane coordination
	intersect = ""
	x = 0
	y = 0
	length = 0
	signal = ""
	ontheroad = 0
	
	def __init__(self,length,signal, x,y):
		self.length = length
		self.signal = signal
		self.x = x
		self.y = y
		self.intersect = signal

	def setontheroad(self,n):
		self.ontheroad = self.ontheroad + n

	def appendqueue(self, auto):
		self.queue.put(auto)

	def popqueue(self):
		vehicle = self.queue.get()
		return vehicle
		


