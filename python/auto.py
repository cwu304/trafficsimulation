import numpy

class auto():

    otime = 0
    dtime = 0
    VehicleID = 0
    length = 0
    lane = ""
    ox = 0
    oy = 0
    dx = 1
    dy = 4
    cx = 0
    cy = 3.5
    speed = 50.0
    signalname =""
    origin = ""

    #car parameters
    P_truck = 0.05
    l_truck = 20
    l_passenger = 15

    #generate intervehicle time interval 
    # rate = 9
    # test = np.random.exponential(rate)
    def __init__(self,VehicleID,origin,EntrancesXY,choices,otime):

        self.VehicleID = VehicleID
        self.origin = origin
        self.otime = otime

        ## origin destination lane
        self.ox = EntrancesXY[origin]["x"]
        self.cx = EntrancesXY[origin]["x"]
        self.oy = EntrancesXY[origin]["y"]
        self.cy = EntrancesXY[origin]["y"]

        #generate destination based on origin
        def weighted_choice(choices):
            r = numpy.random.uniform(low = 0, high = 1)
            upto = 0
            for c, w in choices.iteritems():
                if upto + w > r:
                    return c
                upto += w

        dest = weighted_choice(choices)
        self.dx = EntrancesXY[dest]["x"]
        self.dy = EntrancesXY[dest]["y"]
       
        #generate vehicle length
        r1 = numpy.random.uniform(low = 0, high = 1)
        if r1<= self.P_truck:
            self.length = self.l_truck
        else:
            self.length = self.l_passenger

        #generate lane and signalname
        movement = self.turn()
        direction = self.getdirection()
        #set lanename

        if self.cy <= 0.5 or self.cy == 3.5 or self.cy == 4.5:
            if movement == "lt":
                self.lane = origin+"lt"
            else:
                self.lane = origin
        else:
            self.lane = origin

        #set signalname
        if movement == "rt":
            movement = "tr"
        self.signalname = movement + direction


    def turn(self):  
        cx = self.cx
        cy = self.cy
        dx = self.dx
        dy = self.dy 

        deltay = dy-cy
        deltax = dx-cx

        #if the vehicle is at the right side of peach tree street
        if cx == 1:
            if deltay >0:
                return "rt"
            if deltay ==0:
                return "tr"
            if deltay <0:
                return "lt"
        # if the vehicle is at the left side of peachtree street
        if cx == -1:
            if deltay >0:
                return "lt"
            if deltay ==0:
                return "tr"
            if deltay <0:
                return "rt"
        #if the vehicle is already on the peach tree street
        if cx == 0:
            if abs(deltay) >= 1:
                return "tr"
            if deltay == 0.5:
                if deltax == 0:
                    return "tr"
                if deltax > 0:
                    return "rt"
                if deltax < 0:
                    return "lt"
            if deltay == -0.5:
                if deltax == 0:
                    return "tr"
                if deltax > 0:
                    return "lt"
                if deltax < 0:
                    return "rt"

    def getdirection(self):
        if self.cx == 0:
            return "ns"
        else:
            return "ew"
            
    def setdtime(self,dtime):
        self.dtime = dtime

    def setsignalname(self,lt,tr):
        movement = self.turn()
        direction = self.getdirection()
        if self.cy <= 0.5 or self.cy == 3.5 or self.cy == 4.5:
            if movement == "lt":
                self.signalname = movement + direction
                if lt != "":
                    self.lane = lt
                else :
                    self.lane = tr[0]

            elif movement == "tr":
                self.signalname = "tr" + direction
                self.lane = tr[0]
            else:
                self.signalname = "tr" + direction
                self.lane = tr[-1]

        else:
            self.signalname = "tr" + direction
            if movement == "lt" or movement =="tr":
                self.lane = tr[0]
            else :
                self.lane = tr[-1]
















