import Queue

class signal():

    # signal
    ltns = "green"
    trns = "green"
    ltew = "green"
    trew = "green"

    #signal time
    signaltimeDict = {}
    sequence = []
    signalphase = "0"

    # def __init__(self,ltnsred,ltnsgreen,ltewred,ltewgreen,trnsred,trnsgreen,trewred,trewgreen):
    def __init__(self,dict,sequence,phasecurr):
        
        self.sequence = sequence
        self.signaltimeDict= dict
        self.signalphase = phasecurr
        self.setsignal()

    def setsignal(self):

        if self.signalphase == "0":
            self.ltns = "red"
            self.trns = "red"
            self.ltew = "red"
            self.trew = "red"
        if self.signalphase == "1":
            self.ltns = "green"
            self.trns = "red"
            self.ltew = "red"
            self.trew = "red"
        if self.signalphase == "2":
            self.ltns = "red"
            self.trns = "green"
            self.ltew = "red"
            self.trew = "red"
        if self.signalphase == "3":
            self.ltns = "red"
            self.trns = "red"
            self.ltew = "green"
            self.trew = "red"
        if self.signalphase == "4":
            self.ltns = "red"
            self.trns = "red"
            self.ltew = "red"
            self.trew = "green"

    def setphase(self):       
        currphase = self.signalphase
        nextphase = self.sequence.pop(0)
        self.sequence.append(currphase)
        self.signalphase = nextphase






		